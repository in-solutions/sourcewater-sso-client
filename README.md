# Sourcewater SSO Client


#### Purpose
This client contains methods and an example for easy implementation of 
Sourcewater single sign on (SSO). The client provides methods for authenticating users, 
validating tokens and creating the URL with data required for SSO Login page to 
correctly return to the application.

## Installation
```
composer requre sourcewater/sso-client
```


## Example
After installation of sso-client you can find the example of implementation in 
example folder of this package, 
`example/index.php`.

## Usage
### Required Configuration

 
##### Aplication ID & Application Secret
Every application in SSO Admin has unique token ID and Secret. Application ID is used 
for public authentication of app. 
Secret token is used when validating APP CURL requests.

If application doesn't send secret token, user token can't be validated. 
#####
##### Redirect URL
```php
http://web.page.com
```
Redirect url is addres to which user will be redirected after successfull login to SSO admin page. SSO will attach new token as parameter to this url.
```php
http://web.page.com?token=newTokenString
```

###Create Client
If we have all configuration data set. We can create client.

```php
$config = array(
    'app_id' => 'APPLICATION_ID',
    'app_secret' => 'APPLICATION_SECRET',
    'redirect_url' => 'REDIRECT_URL'
);
```
```php
use Sourcewater/SSO/Client;
$ssoClient = new Client($config);
```
SSO client has preset Authentication Server set to `https://login.sourcewater.com`,
it is possible to change this addres with public client method
```php
$newURL = 'http://localhost:8000';
$ssoClient->setAuthenticationServer( $newURL );
``` 
###Authentication of users
Every logged user receives token from SSO Server. 
If we do not have token stored we can redirect user to 
SSO Login url, we can obtain from sso client instance.
#### Obtain Token
Login URL consist of `app_id` and `redirect_url`. 
Before getting login URL we can change `redirect url` by method setRedirectUrl.
```php
$newRedirectUrl = 'www.newPage.com/different';

$ssoClient->setRedirectUrl( $newRedirectUrl );

$url = $ssoClient->getLoginUrl(); 
// AUTH_SERVER_URL?app_id=APP_ID&redirect_url=REDIRECT_URL 
```
When we redirect to login url SSO server shows user 
login screen of SSO and after successfull login it 
redirects user to `redirect_url` with attached token
 `redirect_url?token=ISSUED_TOKEN`. It is possible 
 to have url with parameters 
 `redirect_url?custom=parameter&token=ISSUED_TOKEN`
 
 ###Authenticate with Received Token
 After application received token application can authenticate user with token
```php
$token = $_GET['token'];
if( $ssoClient->authenticate($token) ){
``` 
####Store Token
After successful authentication we can store 
token to cookie. Validity of token we can get 
with method getTokenExpiration
```php
if(isset($_GET['token'])){
    $tokenValidUntil = $ssoClient->getTokenExpiration();
    setcookie('sw_token', $_GET['token'], strtotime($tokenValidUntil));
}
``` 
####Get User
SSO User data can be retreived from autheticated user with method getUser. 
```php
$user = $ssoClient->getUser()
```

 
###Verify token
 
It is possible just verify token validity without returning user data. This method only confirms if token is valid for application.
```php
$ssoClient->verifyToken( $token )
```
###Caching
 It is possible to cache data from client and 
 insert them to client in next request. If client has has data set it does not fire authenticate CURL action and returns provided cached data.
 
####Get data for Cache
```php
$dataToCache = $ssoClient->getData();

// Store data
$_SESSION['ssoCache'] = $dataToCache;
```
####Set data from Cache
```php
if ( isset($_SESSION['ssoCache']) ){
    
    $data = $_SESSION['ssoCache'];
    // set data to client
    $ssoClient->setData( $data );
}

// when SSO Client is called it will return provided data 
// instead of contacting SSO Server

    $ssoClient->authenticate($token); // Always retruns true
    
    $ssoClient->getUser; // User from cache
```

#### Clear Cache
```php
$ssoClient->setData(null);
```