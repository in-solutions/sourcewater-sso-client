<?php
namespace Sourcewater\SSO;
/* session used for caching the data */
session_start();
require_once __DIR__.'/../Client.php';

$client = new Client([
    'app_id'=>'APP_ID',
    'app_secret'=>'APP_SECRET_KEY',
    'redirect_url'=>'http://localhost/example/index.php'
]);

/*
Application can pass cached data to Client Class,
in this case backend no CURL request is sent and
Client Class works with provided data
*/
if ( isset($_SESSION['sw_cached_data']) ) {
   // $client->setData($_SESSION['sw_cached_data']);
}

$userToken = null;

/* Try to obtain user token */
if (isset($_GET['token'])) {
    $userToken = $_GET['token'];
}
else if(isset($_COOKIE['sw_token'])) {
    $userToken = $_COOKIE['sw_token'];
}
else { /* redirect to SW login url */
   header('Location: '.$client->getLoginUrl()); exit();
}

/* We can verifyToken checks if user is signed in -> it returns info about token expiration */
if ( $client->verifyToken($userToken) ) {
    echo "Token is valid!<br/><br/>";
};

if ($userToken && $client->authenticate($userToken))
{
    /* Obtain token expiration and store user token to cookies */
    if(isset($_GET['token'])){
        $tokenValidUntil = $client->getTokenExpiration();
        setcookie('sw_token', $_GET['token'], strtotime($tokenValidUntil));
    }
    /* Optionally possible to store cache data to Session  */
    $_SESSION['sw_cached_data'] = $client->getData();

    /* user data */
    $user = $client->getUser();


    echo '<b>User:</b> <br />';
    printUser($user);
    echo '<br /><br />';

    /* Check user permission: permission */
    if (!$client->hasPermission('permission')){
        echo 'User without permission: permission!<br /><br />';
        /* no permission code */
    } else {
        echo 'User has permission: permission!<br /><br />';
        /* with permission code */
    }
}
else {
    /* Show link to login page */
    $loginUrl = $client->getLoginUrl();
    echo '<br />To Sourcewater login page: <a href="'.$loginUrl.'">'.$loginUrl.'</a>';
}



function printUser($user)
{
    if (isset($user)) {
        foreach ($user as $key => $data):
            ?>
            <div>
                <?php echo '<b>' . $key . '</b>: ';
                var_dump($data) ?>
            </div>
        <?php endforeach;
    }
}



