<?php
namespace Sourcewater\SSO;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;

class Client {

    protected $auth = 'https://login.sourcewater.com';
    protected $data;
    protected $client;

    public $error = [];

    public function setAuthenticationServer($authUrl){
        $this->auth = $authUrl;
    }

    public function __construct($config)
    {

        if (isset($config['app_id'])) $this->appId = $config['app_id'];
        if (isset($config['app_secret'])) $this->appSecret = $config['app_secret'];
        if (isset($config['redirect_url'])) $this->redirectURL = $config['redirect_url'];

        $this->client = new GuzzleClient([
            'base_uri' => $this->auth,
            'timeout' => 0.0
        ]);

        $this->error = [];
    }

    public function authenticate($token)
    {
        /* Do not request data if data already exist */
        if($this->data){ return true; }

        $params = array(
            'token' => $token,
            'app_secret'=> $this->appSecret,
        );

        try {
            $request = new Request('GET', $this->auth . '/api/client/auth?' . http_build_query($params));
            $response = $this->client->send($request);
            $response = $response->getBody()->getContents();
        } catch (RequestException $e) {
            $response = $e->getMessage();
            $this->error []= $response;
            return false;
        }
        $this->data = json_decode($response);
        return true;
    }

    public function verifyToken($token){
        $params = array(
            'token' => $token,
            'app_secret'=> $this->appSecret,
        );
        try {
            $request = new Request('GET', $this->auth . '/api/client/verify?' . http_build_query($params));
            $response = $this->client->send($request);
            $response = $response->getBody()->getContents();
        } catch (RequestException $e) {
            $response = $e->getMessage();
            $this->error []= $response;
            return null;
        }
        return json_decode($response);
    }

    public function hasPermission($permission)
    {
        return $this->data && $this->data->permissions && $this->data->permissions ?
            in_array($permission, $this->data->permissions) :
            false;
    }

    public function getTokenExpiration(){
        return $this->data && $this->data->token && $this->data->token->expires_at;
    }

    /* Use to set cached data */
    public function setData($data){
        $this->data = $data;
    }

    public function getData(){
        return $this->data;
    }

    public function getUser()
    {
        return $this->data && isset($this->data->user)? $this->data->user : null;
    }

    public function setRedirectUrl($redirectUrl){
        $this->redirectURL = $redirectUrl;
    }

    public function getLoginUrl()
    {
        $params = array(
            'app_id' => $this->appId,
            'redirect_url' => $this->redirectURL,
        );
        return $this->auth . '?' . http_build_query($params);
    }

}